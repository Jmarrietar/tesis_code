%%%%%%%%%%Musk1%%%%%%%%%%%%%%
%a = prdataset('musk1');
%w = scalem(a,'variance');
%a = a*w;

%%%%%%%%%Musk2%%%%%%%%%%%%%%%
%a = prdataset('musk2');
%w = scalem(a,'variance'); 
%a = a*w;

%a = prdataset('elephant');
%a = prdataset('fox');
%a = prdataset('tiger');
%a = prdataset('mutagenesis1');
%a = prdataset('mutagenesis2');
%a=prdataset('web1') 
%a=prdataset('web2') 
%%%%%%%Birds%%%%%%%
x = prdataset('birds');
a=changelablist(x,'WIWR')
%a = changelablist(x,'BRCR');
a = genmil(a.data, a.labels, getbagid(a));
%a=prdataset('birds') 
%a=changelablist(a,'WIWR')
%a=changelablist(a,'BRCR')

%%%%%%COREL%%%%%%%%%%
%x = prdataset('corel');
%a=changelablist(x,'African')
%a = changelablist(x,'Cars');
%a = genmil(a.data, a.labels, getbagid(a));


%Mil2Balu
[X,d,Bag] =mil2balu(a);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%Balancear Datos [instancias]%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Algoritmos para Balancear instancias por Bolsa (Maximo,Minimo,Promedio)
%[X,d,Bag]=GenerarInstancias(X,d,Bag);
%[X,d,Bag]=EliminarInstancias(X,d,Bag);
[X,d,Bag]=PromedioInstancias(X,d,Bag);

%%%%%%%%%%%%%%%%%%%%
%Balancear con OSSj%
%%%%%%%%%%%%%%%%%%%%
%Note: Se usa CNN. 
%UNDER-SAMPLING method, where the label for the majority class is 0 
% nd the label for the minority class is 1.
%Check this rule. 
%C = size(X(d==1),1);  %Tener un listado de los Ids de las Bolsas positivas
%E = size(X(d==0),1);   %Tener un listado de los Ids de las Bolsas negativas
%if (E>C)
%[X,d,Bag] = Bds_OSSJ(X,d,Bag); % Balancear con OSSJ Adaptado a instancias de Bolsas. 
%else 
%    disp('Error: No se cumple que insta - > insta +')
%    break;
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%
%%Balancear Bolsas %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
%[X,d,Bag]=BalanceoBolsas(X,d,Bag);

%BALU2MIL
%Creando MIL Data y Mil labels
d_mil = genmillabels(d, 1);
a = genmil(X,d_mil,Bag);       

%If necesary prmemory(1000000000000000000000000)
%settings for classification:
reg = 1e-6;
w = {
    %apr_mil([],'presence',75,0.999,0.001,1);  %Listo  
    %milboostc([]);                            %Listo [default=100 rounds]
    %citation_mil([],'presence',1,1);          %Listo [ref1, cite1 (h) Frac= presence]
    %citation_mil([],'presence',3,5);          %Listo [ref3, cite5 (h)]
    %misvm([],'presence',1,'p',1);             %Listo p=1  LENTO 4/10
    %misvm([],'presence',1,'r',10);            %Malo (musk1) r=10  Muy diferente que en pagina. 
    miles([],1,'p',1);                        %Listo p=1
    %miles([],1,'r',10);                       %Malo (musk1) r=10      MUY diferente al de la pagina (musk1)
    %simple_mil([],'presence',loglc);          %PROBAR CON LOGLC, LDC,QDCs
    %simple_mil([], 'presence', scalem('variance')*libsvc([],proxm('p',2),0.01)*classc);
    %milvector([],'m')*scalem([],'variance')*loglc([])*classc; %mean-inst+Logistic2 
    %milvector([],'e')*scalem([],'variance')*loglc([])*classc; %extremes+Logistic2
    %milvector([],'c')*scalem([],'variance')*loglc([])*classc; %DEMORADO!!<- cov-coef+Logistic2 [No pasa de 1]
};
wnames = getwnames(w);

%set other parameters and storage:
nrfolds = 10;
nrw = length(w);
err = repmat(NaN,[nrw 2 nrfolds]);

% start the loops:
I = nrfolds;
for i=1:10   %Cambie yo aqui 
    % Randomize the data set
    a = milrandomize(a);
    
	dd_message(3,'%d/%d ',i,10);  %cambie yo aqui
	[x,z,I] = milcrossval(a,I);  % a Mil data set, I # folds, x train mil dataset, z test , I udated objecto noseque.  
    z = setprior(z,[]);
    
	for j=1:nrw
		dd_message(4,'.');
		w_tr = x*w{j};   %se le aplica clasificador a x ? 
		out = z*w_tr;   
        err(j,1,i) = dd_auc(out*milroc);  %Que hace cada parte de esto. 
        err(j,2,i) = out*testd;          %Que es out y que es testd
            metrics = num2cell(mil_evaluation(out));
            [TP, FN, FP, TN, P, R, F, G] = metrics{:};
            err(j,3,i) = F;
            err(j,4,i) = G;
	end
end
dd_message(3,'\n');



result = zeros(nrw,4);
for j=1:nrfolds
	result=result+err(:,:,j);
end
result=(result/nrfolds)*100;

result

